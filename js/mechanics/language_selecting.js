define([
    "utils/input",
    "utils/save_data"
], function(Input, SaveData) {
   const language_choice_key = "_choice_language";

    function Language(context, name, stats) {
        const self = this;

        self.name = name;
        self.stats = stats;

        self.apply = function() {
            for(var stat in self.stats)
                context.statsManaging[stat].thresholdValue(self.stats[stat] || 0.0);

            SaveData.boolProperty(language_choice_key, name);
        };
    }

    function LanguageSelecting(context) {
        // -- Private fields

        const self = this;

        // -- Public fields

        self.languages = null;

        // -- Private functions

        var initializeLanguages = function() {
            self.languages = [
                new Language(context, "Ruby", {
                    productivity: 0.3,
                    happiness: 0.5,
                    concentration: 0.4
                }),
                new Language(context, "JavaScript", {
                    productivity: 0.5,
                    happiness: 0.4,
                    concentration: 0.2
                }),
                new Language(context, "Python", {
                    productivity: 0.2,
                    happiness: 0.5,
                    concentration: 0.4
                }),
                new Language(context, "FORTRAN", {
                    productivity: 0.1,
                    happiness: 0.1,
                    concentration: 0.1
                })
            ];
        };

        // -- Public functions

        this.create = function() {
            initializeLanguages();
        };
    }

    return LanguageSelecting;
});
