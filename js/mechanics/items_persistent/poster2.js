define(function() {
    function Poster2(context) {
        // -- Private fields

        const self = this;
        var productivity_bonus = 0.2;

        // -- Public fields

        self.name = "poster2";
        self.price = 250;

        self.title = "Cheatsheet";
        self.description = "A poster with a cheatsheet of your programming language. " +
                           "Admit it: you won't event bother to read it if you purchase it. " +
                           "Improves productivity.";

        self.display = {
            x: 0,
            y: -55,
            layer: "backgroundLayer",
            image: "item_poster2"
        };

        // -- Public functions

        self.onUnlock = function() {
            var attr = context.statsManaging.productivity;
            attr.thresholdValue(attr.thresholdValue() + productivity_bonus);
        };
    }

    return Poster2;
});
