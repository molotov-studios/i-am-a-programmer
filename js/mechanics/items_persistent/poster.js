define(function() {
    function Poster(context) {
        // -- Private fields

        const self = this;
        var happiness_bonus = 0.1;

        // -- Public fields

        self.name = "poster";
        self.price = 150;

        self.title = "Programming poster";
        self.description = "A poster with a joke related to smoke testing. " +
                           "It's extremely funny. You should read it on the poster. " +
                           "Improves happiness.";

        self.display = {
            x: 35,
            y: -50,
            layer: "backgroundLayer",
            image: "item_poster"
        };

        // -- Public functions

        self.onUnlock = function() {
            var attr = context.statsManaging.happiness;
            attr.thresholdValue(attr.thresholdValue() + happiness_bonus);
        };
    }

    return Poster;
});
