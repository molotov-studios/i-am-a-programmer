define(function() {
    function Rubik(context) {
        // -- Private fields

        const self = this;
        var productivity_bonus = 0.11;

        // -- Public fields

        self.name = "rubik";
        self.price = 100;

        self.title = "Fake Rubik";
        self.description = "A chinese version of a rubik's cube. " +
                           "It's probably best used as fuel for a fireplace. " +
                           "Improves productivity.";

        self.display = {
            x: -74,
            y: -2,
            layer: "backgroundLayer",
            image: "item_rubik"
        };

        // -- Public functions

        self.onUnlock = function() {
            var attr = context.statsManaging.productivity;
            attr.thresholdValue(attr.thresholdValue() + productivity_bonus);
        };
    }

    return Rubik;
});
