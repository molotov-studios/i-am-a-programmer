define(function() {
    function Books(context) {
        // -- Private fields

        const self = this;
        var concentration_bonus = 0.1;

        // -- Public fields

        self.name = "books";
        self.price = 80;

        self.title = "Old books";
        self.description = "A bunch of old programming books. " +
                           "Once bought, you won't read them. Ever. "; +
                           "Web spider included. "; +
                           "Improves concentration.";

        self.display = {
            x: -120,
            y: 44,
            layer: "backgroundLayer",
            image: "item_books"
        };

        // -- Public functions

        self.onUnlock = function() {
            var attr = context.statsManaging.concentration;
            attr.thresholdValue(attr.thresholdValue() + concentration_bonus);
        };
    }

    return Books;
});
