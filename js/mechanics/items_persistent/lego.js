define(function() {
    function Lego(context) {
        // -- Private fields

        const self = this;
        var productivity_bonus = 0.15;

        // -- Public fields

        self.name = "lego";
        self.price = 300;

        self.title = "Lego head";
        self.description = "An amazing lego head with no useful purpose. " +
                           "Improves productivity.";

        self.display = {
            x: -78,
            y: -48,
            layer: "backgroundLayer",
            image: "item_lego"
        };

        // -- Public functions

        self.onUnlock = function() {
            var attr = context.statsManaging.productivity;
            attr.thresholdValue(attr.thresholdValue() + productivity_bonus);
        };
    }

    return Lego;
});
