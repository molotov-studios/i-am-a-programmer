define(function() {
    function Duck(context) {
        // -- Private fields

        const self = this;
        var concentration_bonus = 0.1;

        // -- Public fields

        self.name = "duck";
        self.price = 200;

        self.title = "Debugging duck";
        self.description = "Useful item to debug code by explaining it to a rubber duck. " +
                           "Improves concentration.";

        self.display = {
            x: -112,
            y: -48,
            layer: "backgroundLayer",
            image: "item_duck"
        };

        // -- Public functions

        self.onUnlock = function() {
            var attr = context.statsManaging.concentration;
            attr.thresholdValue(attr.thresholdValue() + concentration_bonus);
        };
    }

    return Duck;
});
