define([
    "utils/math",
    "utils/save_data",
    "utils/unlockable"
], function(MathUtils, SaveData, Unlockable) {
    function BugSpawning(context) {
        // -- Private fields

        const self = this;

        const time_range = { min: 10000, max: 80000 };
        const time_range_add_by_concentration = 15000;

        const min_auto_bugs = { min: 6, max: 10 };
        const min_auto_bugs_sub_by_concentration = 4;

        const user_data_bugs_property = "_bugs_pending_count";

        var bugs_pending = null;
        var spawn_timer = null;

        // -- Public fields

        self.onNewBug = new Phaser.Signal();
        self.onBugSolved = new Phaser.Signal();
        self.onAllBugsSolved = new Phaser.Signal();

        // -- Private functions

        var randomTime = function() {
            var factor = context.statsManaging.concentration.totalValue();
            var extra = Math.floor(time_range_add_by_concentration * factor);
            return extra + MathUtils.randomIntInRange(time_range.min, time_range.max);
        };

        var randomBugsCount = function() {
            var factor = context.statsManaging.concentration.totalValue();
            var sub = Math.floor(min_auto_bugs_sub_by_concentration * factor);

            return MathUtils.randomIntInRange(min_auto_bugs.min, min_auto_bugs.max + 1) - sub;
        };

        var autoSpawn = function() {
            if(spawn_timer == null) {
                spawn_timer = context.game.time.create(false);
                spawn_timer.start();
            }
            else {
                spawn_timer.stop();
                spawn_timer.start();
                self.spawn(randomBugsCount());
            }

            var time = randomTime();
            console.debug(`Next bug batch will spawn in ${time/1000} seconds.`);
            spawn_timer.add(time, autoSpawn);
        };

        var incrementBugsCount = function(by) {
            bugs_pending += by;
            if(bugs_pending < 0) bugs_pending = 0;

            SaveData.intProperty(user_data_bugs_property, bugs_pending);

            if(by > 0) {
                self.onNewBug.dispatch(by);
            }
            else if(by < 0) {
                self.onBugSolved.dispatch(by);
                if(bugs_pending == 0)
                    self.onAllBugsSolved.dispatch(by, 'bugs_solved');
            }
        };

        // -- Public functions

        self.create = function() {
            if(self.isUnlocked())
                bugs_pending = SaveData.intProperty(user_data_bugs_property, undefined, 0);
            else
                bugs_pending = 0;
        };

        self.pause = function() {
            if(self.isLocked())
                return;

            if(spawn_timer != null)
                spawn_timer.pause();
        };

        self.resume = function() {
            if(self.isLocked())
                return;

            if(spawn_timer != null) {
                spawn_timer.resume();
            }
            else {
                autoSpawn();
                if(bugs_pending > 0) {
                    self.onNewBug.dispatch(bugs_pending);
                }
            }
        };

        self.spawn = function(count) {
            if(!count)
                count = 1;

            incrementBugsCount(count);
        };

        self.solve = function(count) {
            if(!count)
                count = 1;

            incrementBugsCount(-count);
        };

        self.bugsPending = function() {
            return bugs_pending > 0;
        };

        self.bugsPendingCount = function() {
            return bugs_pending;
        };

        // -- Extend behaviour
        Unlockable(self, self.resume);
    }

    return BugSpawning;
});
