define([
    "mechanics/random_events/bugs",
    "mechanics/random_events/credits_earn",
    "mechanics/random_events/credits_loss",
    "mechanics/random_events/spam",
    "mechanics/random_events/stats_loss",
    "mechanics/random_events/trade",
    "mechanics/random_events/tutorial",

    "utils/math",
    "utils/unlockable"
], function(
    BugsEvent,
    CreditsEarnEvent,
    CreditsLossEvent,
    SpamEvent,
    StatsLossEvent,
    TradeEvent,
    TutorialEvent,

    MathUtils,
    Unlockable
) {
    function RandomEventCalling(context) {
        // -- Private fields

        const self = this;

        const time_range = { min: 60000.0, max: 120000.0 };
        const time_range_sub_by_happiness = 40000.0;

        var current_timeout = null;
        var timer = null;

        const random_events = [
            new BugsEvent(self, context),
            new CreditsEarnEvent(self, context),
            new CreditsLossEvent(self, context),
            new SpamEvent(self, context),
            new StatsLossEvent(self, context),
            new TradeEvent(self, context),
            new TutorialEvent(self, context)
        ];

        // -- Public fields

        self.onEventCompleted = new Phaser.Signal();

        // -- Private functions

        var selectRandomEvent = function() {
            return MathUtils.randomInListWithWeights(random_events);
        };

        var randomTime = function() {
            var sub = context.statsManaging.happiness.totalValue() * time_range_sub_by_happiness;
            return MathUtils.randomInRange(time_range.min, time_range.max) - sub;
        };

        var executeRandomEvent = function(event) {
            context.pauseIngameMechanics();

            if(!event)
                event = selectRandomEvent();

            event.execute();
        };

        var prepareNextCall = function(event, now) {
            clearNextCall();

            var time = randomTime();

            if(now) {
                executeRandomEvent(event);
            }
            else {
                timer = context.game.time.create(false);
                console.debug(`Next event will occur in ${time/1000} seconds.`);
                timer.add(time, executeRandomEvent, self, event);
                timer.start();
            }
        };

        var clearNextCall = function() {
            if(timer != null) {
                timer.stop();
                timer = null;
            }
        };

        // -- Public functions

        self.create = function() {
            // Do nothing
        };

        self.shutdown = function() {
            clearNextCall();
        };

        self.enqueue = function(event, now) {
            if(self.isLocked())
                return;

            prepareNextCall(event, now);
        };

        self.events = function() {
            return random_events;
        };

        self.onEventSolved = function(accepted) {
            // When an event is solved (accepted or declined), generate a new event.
            context.resumeIngameMechanics();
            self.onEventCompleted.dispatch(accepted);
            self.enqueue();
        };

        self.pause = function() {
            if(timer)
                timer.pause();
        };

        self.resume = function() {
            if(timer)
                timer.resume();
        };

        self.findAndExecute = function(name) {
            var event = random_events.find(function(event) {
                return event.name == name;
            });
            self.enqueue(event, true);
        };

        // -- Extend behaviour

        Unlockable(self, self.enqueue);
    }

    return RandomEventCalling;
});
