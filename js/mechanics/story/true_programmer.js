define(function() {
    function TrueProgrammer(context) {
        // -- Private fields

        const self = this;
        const final_name = "true_programmer";

        const keySequence = {
            // You found the secret ending sequence! Please, do NOT share it with other players :]
            sequence: "javascript is for pussies",
            callback: function() {
                context.storying.triggerFinal(final_name);
            }
        };

        // -- Public fields

        self.name = final_name;

        // -- Private functions

        var setupSequence = function() {
            context.keySequencing.register(keySequence);
        };

        // -- Public functions

        self.setup = function() {
            setupSequence();
        };
    }

    return TrueProgrammer;
});
