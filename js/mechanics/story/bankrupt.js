define([
    "utils/save_data"
], function(SaveData) {
    function Bankrupt(context) {
        // -- Private fields

        const self = this;
        const credits_to_trigger = -20000;
        const final_name = "bankrupt";

        const warning_property = '_bankrupt_warning_phrase';
        const warnings = [
            {
                value: -100,
                text: "Please, try not to spend too much money; it does not grow on trees."
            },
            {
                value: -5000,
                text: "Wow, you're starting to generate some serious debts..."
            },
            {
                value: -10000,
                text: "Do you want us to go bankrupt?! You better recover that money!"
            }
        ];

        // -- Public fields

        self.name = final_name;

        // -- Private functions

        var setupEvents = function() {
            context.creditsManaging.onCreditsChange.add(checkCredits);
        };

        var checkCredits = function() {
            var credits = context.creditsManaging.credits();

            if(credits <= credits_to_trigger) {
                execute();
            }
            else {
                for(i in warnings) {
                    var warning = warnings[i];
                    var step = SaveData.intProperty(warning_property);

                    if(credits <= warning.value && step == i) {
                        SaveData.intProperty(warning_property, step + 1);
                        context.state.storyMessages.print(warning.text);
                        return;
                    }
                }
            }
        };

        var execute = function() {
            context.storying.triggerFinal(final_name);
        };

        // -- Public functions

        self.setup = function() {
            SaveData.intProperty(warning_property, undefined, 0);
            setupEvents();
        };
    }

    return Bankrupt;
});
