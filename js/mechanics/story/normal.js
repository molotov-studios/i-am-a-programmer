define([
    "utils/save_data"
], function(SaveData) {
    function Normal(context) {
        // -- Private fields

        const self = this;
        const save_property = "_current_normal_quest";
        const quests = [
            {
                name: "write_first_loc",
                setup: function() {
                    print("Welcome to your first day, programmer! Are you ready?");
                    print(
                        "Your first task is to write one line of code. Go on! Don't be shy!",
                        null,
                        function() {
                            context.creditsEarning.unlock();
                            context.coding.unlock();
                            context.coding.onLineCompleted.add(current_quest.onComplete);
                        }
                    );
                },
                onComplete: function() {
                    context.coding.onLineCompleted.remove(current_quest.onComplete);
                    advanceQuest();
                }
            },
            {
                name: "write_more_loc",
                setup: function() {
                    print(`Excelent! Go ahead, write ${current_quest.goal} more lines.`);
                    context.coding.unlock();
                    context.coding.onLineCompleted.add(current_quest.checkCompletion);
                },
                goal: 5,
                count: 0,
                checkCompletion: function() {
                    current_quest.count++;
                    if(current_quest.count >= current_quest.goal)
                        current_quest.onComplete();
                },
                onComplete: function() {
                    context.coding.onLineCompleted.remove(current_quest.checkCompletion);
                    advanceQuest();
                }
            },
            {
                name: "solve_bugs",
                fixBugTutorial: function() {
                    print("Well, look at that bug! It's extremely important to fix bugs as soon as possible. Come on, smash that bug!");
                    context.bugSpawning.onAllBugsSolved.add(current_quest.onComplete);
                },
                setup: function() {
                    if(context.bugSpawning.isUnlocked()) {
                        current_quest.fixBugTutorial();
                    }
                    else {
                        print(`You're already a master on this!`, null, function() {
                            context.bugSpawning.spawn(1);
                            current_quest.fixBugTutorial();
                        });
                    }
                },
                onComplete: function() {
                    context.bugSpawning.unlock();
                    context.bugSpawning.onAllBugsSolved.remove(current_quest.onComplete);
                    advanceQuest();
                }
            },
            {
                name: "earn_money",
                goal: 2000,
                setup: function() {
                    print(`Greay! Now that you know the basics, you can start making some money. Your goal is to earn ${current_quest.goal}$`);
                    context.creditsManaging.onCreditsChange.add(current_quest.checkCompletion);
                },
                checkCompletion: function(credits) {
                    if(credits >= current_quest.goal)
                        current_quest.onComplete();
                },
                onComplete: function() {
                    context.creditsManaging.onCreditsChange.remove(current_quest.checkCompletion);
                    advanceQuest();
                }
            },
            {
                name: "solve_random_event",
                setup: function() {
                    print("You're gonna make us rich! It's time for you to dev-ops and take care of other tasks (also known as random events).", null, function() {
                        context.randomEventCalling.unlock();
                        context.randomEventCalling.onEventCompleted.add(current_quest.onComplete);
                        context.randomEventCalling.findAndExecute("tutorial");
                    });
                },
                onComplete: function() {
                    context.randomEventCalling.onEventCompleted.remove(current_quest.onComplete);
                    advanceQuest();
                }
            },
            {
                name: "purchase_one_item",
                setup: function() {
                    print("Amazing! Oh, by the way, your office is kinda empty, isn't it? Why don't you buy some decorations for it?");
                    context.persistentShopping.unlock();
                    context.persistentShopping.onItemUnlocked.add(current_quest.onComplete);
                },
                onComplete: function() {
                    context.persistentShopping.onItemUnlocked.remove(current_quest.onComplete);
                    print(`Well, I was thinking on a plant or something... Anyways, that will work for now.`);
                    advanceQuest();
                }
            },
            {
                name: "purchase_all_items",
                setup: function() {
                    print("Your next task is to FULLY decorate the office. We need a cozy work place for future coworkers.");
                    context.persistentShopping.onAllItemsUnlocked.add(current_quest.onComplete);
                },
                onComplete: function() {
                    print(`Woah, it looks like a totally different place!`);
                    context.persistentShopping.onAllItemsUnlocked.remove(current_quest.onComplete);
                    advanceQuest();
                }
            },
            {
                name: "earn_tons_of_money",
                goal: 40000,
                setup: function() {
                    print(`We're finally ready! Time to make us rich! Your goal is to earn nothing less than ${current_quest.goal}$`);
                    context.creditsManaging.onCreditsChange.add(current_quest.checkCompletion);
                },
                checkCompletion: function(credits) {
                    if(credits >= current_quest.goal)
                        current_quest.onComplete();
                },
                onComplete: function() {
                    context.creditsManaging.onCreditsChange.remove(current_quest.checkCompletion);
                    advanceQuest();
                }
            },
            {
                name: "ending",
                setup: function() {
                    print("Excelent. A decorated office, lots of money and a nice duck figure. You know what?");
                    print("I don't need you anymore. In fact, you're already fired. Thank you for being my slave. Have fun sleeping under a bridge.", null, function() {
                        current_quest.onComplete();
                    });
                },
                onComplete: function() {
                    context.storying.triggerFinal(self.name);
                }
            }
        ];

        var current_quest = null;
        var current_quest_index = 0;

        // -- Public fields

        self.name = "grind";

        // -- Private functions

        var print = function() {
            context.state.storyMessages.print(...arguments);
        };

        var advanceQuest = function() {
            current_quest = quests[++current_quest_index];
            current_quest.setup();
            SaveData.property(save_property, current_quest.name);
        };

        var setupCurrentQuest = function() {
            quests[current_quest_index].setup();
        };

        var loadCurrentQuest = function() {
            var name = SaveData.property(save_property, undefined, quests[0].name);
            current_quest_index = quests.findIndex(function(quest) { return quest.name == name; });
            current_quest = quests[current_quest_index];
        };

        var execute = function() {
            context.storying.triggerFinal(self.name);
        };

        // -- Public functions

        self.setup = function() {
            loadCurrentQuest();
            setupCurrentQuest();
            // ...
        };
    }

    return Normal;
});
