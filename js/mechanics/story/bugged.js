define([
    "utils/save_data"
], function(SaveData) {
    function Bugged(context) {
        // -- Private fields

        const self = this;
        const bugs_to_trigger = 50;
        const final_name = "bugged";

        const warning_property = '_bugged_warning_phrase';
        const warnings = [
            {
                count: 20,
                text: "So... are you gonna do something about those bugs?"
            },
            {
                count: 40,
                text: "Hey! There are too many bugs! Fix them before it's too late!"
            }
        ];

        // -- Public fields

        self.name = final_name;

        // -- Private functions

        var setupEvents = function() {
            context.bugSpawning.onNewBug.add(checkBugs);
        };

        var checkBugs = function() {
            var count = context.bugSpawning.bugsPendingCount();

            if(count >= bugs_to_trigger) {
                execute();
            }
            else {
                for(i in warnings) {
                    var warning = warnings[i];
                    var step = SaveData.intProperty(warning_property);

                    if(count >= warning.count && step == i) {
                        SaveData.intProperty(warning_property, step + 1);
                        context.state.storyMessages.print(warning.text);
                        return;
                    }
                }
            }
        };

        var execute = function() {
            context.storying.triggerFinal(final_name);
        };

        // -- Public functions

        self.setup = function() {
            SaveData.intProperty(warning_property, undefined, 0);
            setupEvents();
        };
    }

    return Bugged;
});
