define([
    "utils/math",
    "utils/save_data"
], function(MathUtils, SaveData) {
    function CreditsManaging(context) {
        // -- Private fields

        const self = this;
        const initial_credits = 0;
        const credits_property_name = "_player_credits";
        const credits_min = -99999;
        const credits_max = 99999;

        var credits = 0;

        // -- Public fields

        self.onCreditsChange = new Phaser.Signal();
        // ...

        // -- Private functions

        var load = function() {
            credits = SaveData.intProperty(credits_property_name, undefined, initial_credits);
        };

        // -- Public functions

        self.create = function() {
            load();
        };

        self.removeCredits = function(val, reason) {
            self.addCredits(-val, reason);
        };

        self.addCredits = function(val, reason) {
            var prev = SaveData.intProperty(credits_property_name);
            credits = SaveData.intProperty(
                credits_property_name,
                MathUtils.clamp(prev + val, credits_min, credits_max)
            );
            self.onCreditsChange.dispatch(credits, val, reason);
        };

        self.enoughCredits = function(amount) {
            return credits >= amount;
        };

        self.credits = function() {
            return credits;
        };
    }

    return CreditsManaging;
});
