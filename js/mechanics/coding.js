define([
    "utils/math",
    "utils/mechanics",
    "utils/unlockable"
], function(MathUtils, Mechanics, Unlockable) {
    function Coding(context) {
        // -- Private fields

        const self = this;
        const enter_key = "Enter";
        const min_line_length = 20;
        const max_line_length = 40;
        const spam_blocked_delay = 200;

        var expected_line_length = 0; // Target length
        var typed_characters = 0;
        var spam_blocked_change_timer = null;

        // -- Public fields

        self.onRestartLine = new Phaser.Signal();
        self.onLineChange = new Phaser.Signal();
        self.onLineReady = new Phaser.Signal();
        self.onLineCompleted = new Phaser.Signal();
        self.onBlockedChange = new Phaser.Signal();

        // -- Private functions

        var restartCurrentTargetLength = function() {
            expected_line_length = MathUtils.randomIntInRange(min_line_length, max_line_length);
            typed_characters = 0;
            self.onRestartLine.dispatch(expected_line_length);
        };

        var enoughCharacters = function() {
            return typed_characters >= expected_line_length;
        };

        var carriageReturn = function(char) {
            return char == enter_key;
        };

        var onKeystroke = function(event) {
            if(self.isLocked())
                return;

            // Ignore if there are some bugs present!
            if(context.bugSpawning.bugsPending()) {
                // Check timer before dispatching to avoid spaming!
                if(spam_blocked_change_timer == null) {
                    setupAntiSpamBlockedChange();
                    self.onBlockedChange.dispatch(context.bugSpawning.bugsPendingCount(), "bug");
                }
                else if(spam_blocked_change_timer.ms > spam_blocked_delay) {
                    spam_blocked_change_timer.stop();
                    spam_blocked_change_timer.start();
                    self.onBlockedChange.dispatch(context.bugSpawning.bugsPendingCount(), "bug");
                }

                return;
            }

            if(!enoughCharacters() && !carriageReturn(event.key)) {
                typed_characters += 1;
                self.onLineChange.dispatch(typed_characters, expected_line_length);
                if(enoughCharacters())
                    self.onLineReady.dispatch(expected_line_length);
            }

            if(enoughCharacters() && carriageReturn(event.key)) {
                var aux = expected_line_length;
                restartCurrentTargetLength();
                self.onLineCompleted.dispatch(aux);
            }
        };

        var setupAntiSpamBlockedChange = function() {
            spam_blocked_change_timer = context.game.time.create(false);
            spam_blocked_change_timer.start();
        };

        // -- Public functions

        self.create = function() {
            context.keyStroking.onKeystroke.add(onKeystroke);
            restartCurrentTargetLength();
        };

        self.currentLineFinished = function() {
            return typed_characters == expected_line_length;
        };

        // -- Extend behaviour
        
        Unlockable(self);
    }

    return Coding;
});
