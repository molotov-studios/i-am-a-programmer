define([
    "utils/math",
    "utils/mechanics",
    "utils/save_data",
    "utils/time",
    "utils/unlockable"
], function(MathUtils, Mechanics, SaveData, Time, Unlockable) {
    function CreditsEarning(context) {
        // -- Private fields

        const self = this;
        const base_credits_per_line = 100;
        const extra_credits_per_line_due_productivity = 120;
        const base_credits_per_iddle_minute = 2;
        const extra_credits_per_iddle_minute_due_productivity = 2;

        const interval_time = 20000;
        const interval_checkpoint_property_name = "_player_last_checkpoint";
        const interval_min_time = 60000;

        var interval_timer;
        var interval_checkpoint;

        // -- Public fields

        // ...

        // -- Private functions

        // Iddle

        var lastIntervalCheckpoint = function() {
            return interval_checkpoint;
        };

        var basePricePerIddleMinute = function(minutes) {
            return MathUtils.round(base_credits_per_iddle_minute * minutes);
        };

        var extraPricePerIddleMinute = function(minutes) {
            return MathUtils.round(extra_credits_per_iddle_minute_due_productivity * minutes);
        };

        var pricePerIddleMinute = function(minutes) {
            return basePricePerIddleMinute(minutes) + extraPricePerIddleMinute(minutes);
        };

        var currentIddleTimeInMinutes = function() {
            return Time.millisecondsToMinutes(Time.current() - lastIntervalCheckpoint());
        };

        var saveIntervalCheckpoint = function() {
            interval_checkpoint = SaveData.intProperty(
                interval_checkpoint_property_name,
                Time.current()
            );
        };

        var onInterval = function() {
            if(enoughElapsedTime()) {
              var credits = pricePerIddleMinute(currentIddleTimeInMinutes());
              saveIntervalCheckpoint();
              context.creditsManaging.addCredits(credits, "interval");
            }
        };

        var createIntervalTimer = function() {
            interval_timer = context.game.time.create(false);
            interval_timer.loop(interval_time, onInterval, self);
            interval_timer.start();
            onInterval();
        };

        var enoughElapsedTime = function() {
            return currentIddleTimeInMinutes() >= Time.millisecondsToMinutes(interval_min_time);
        };

        var loadLastIntervalCheckpoint = function() {
            if(self.isLocked())
                return;

            interval_checkpoint = SaveData.intProperty(
                interval_checkpoint_property_name,
                undefined,
                Time.current()
            );
        };

        // LOC

        var basePricePerLine = function(_lenth) {
            return base_credits_per_line;
        };

        var extraPricePerFile = function(_lenth) {
            var factor = context.statsManaging.productivity.totalValue();
            return MathUtils.round(factor * extra_credits_per_line_due_productivity);
        };

        var pricePerLine = function(length) {
            return basePricePerLine(length) + extraPricePerFile(length);
        };

        var onLineCompleted = function(length) {
            context.creditsManaging.addCredits(pricePerLine(length), "loc");
        };

        // -- Public functions

        self.pause = function() {
            if(self.isLocked())
                return;

            interval_timer.pause();
        };

        self.resume = function() {
            if(self.isLocked())
                return;

            if(interval_timer)
                interval_timer.resume();
            else {
                createIntervalTimer();
            }
        };

        self.create = function() {
            context.coding.onLineCompleted.add(onLineCompleted);
            loadLastIntervalCheckpoint();

            self.onUnlocked.add(function() {
                loadLastIntervalCheckpoint();
                self.resume();
            });
        };

        // -- Extend behaviour

        Unlockable(self);
    }

    return CreditsEarning;
});
