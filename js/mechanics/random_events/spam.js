define([
    "graphics/gui/panel_factory",
    "utils/math"
], function(PanelFactory, MathUtils) {
    function Spam(random_event_calling, context) {
        // -- Private fields

        const self = this;

        const messages = [
            {
                sound: "install_extension",
                accept_text: "Add",
                message: "-------- ======== ******** ======== --------\n\n" +
                         "ERROR: Your Flash Player IS NOT INSTALLED.\n" +
                         "Click 'Add' button to install the extension.\n\n" +
                         "-------- ======== ******** ======== --------"
            },
            {
                sound: "sassy",
                accept_text: "Money!",
                message: "-------- ======== ******** ======== --------\n\n" +
                         "You won't believe how easy is to make money with this method!\n" +
                         "Open this window to earn more momey!\n\n" +
                         "-------- ======== ******** ======== --------"
            }
        ];

        var panel = null;
        var audio = null;

        // -- Public fields

        self.weight = 10;
        self.name = "spam";


        // -- Private functions

        var stopAudio = function() {
            audio.destroy();
        };

        var playAudio = function(id) {
            audio = context.state.game.add.audio(id);
            audio.loop = true;
            audio.play();
        };

        var onAccept = function() {
            random_event_calling.onEventSolved(true);
            stopAudio();
            context.state.game.state.start("BSOTD");
            context.bugSpawning.spawn(40);
        };

        var onDecline = function() {
            random_event_calling.onEventSolved(false);
            stopAudio();
            panel.hide();
        };

        // -- Public functions

        this.execute = function() {
            var random = MathUtils.randomInList(messages);
            panel = PanelFactory.generate(context.state.guiLayer, PanelFactory.POPUP, {
                title: "totally_not_a_virus.exe",
                body: random.message,
                buttons: [
                    { text: random.accept_text, callback: onAccept },
                    { text: random.accept_text, callback: onAccept },
                    { text: "Cancel", callback: onDecline }
                ]
            });
            panel.show();
            panel.root.x = -panel.root.width * 0.5;

            playAudio(random.sound);
        };
    }

    return Spam;
});
