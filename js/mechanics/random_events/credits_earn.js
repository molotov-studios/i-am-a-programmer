define([
    "graphics/gui/panel_factory",
    "utils/math"
], function(PanelFactory, MathUtils) {
    function CreditsEarn(random_event_calling, context) {
        // -- Private fields

        const self = this;
        const percentage_ranges = { min: 0.1, max: 1.2 };
        const min_credits_loss = 100;
        const messages = [
            {
                text: "The military airforces are purchasing your software to use it on" +
                      "ballistic air-to-air missiles.",
                earn_ratio: 0.2
            },
            {
                text: "You have mined some bitcoins successfully.",
                earn_ratio: 0.1
            },
            {
                text: "Feetbook wants to buy some core parts of your software.",
                earn_ratio: 0.3
            }
        ];

        var panel = null;

        // -- Public fields

        self.weight = 25;
        self.name = "credits_earn";

        // -- Private functions

        var creditsMessage = function(message, credits) {
            return `${message}\nYou earn ${credits} $.`;
        };

        var calculateCredits = function(data) {
            var current = Math.floor(data.earn_ratio * context.creditsManaging.credits());
            return Math.max(min_credits_loss, current);
        };

        var onAccept = function() {
            random_event_calling.onEventSolved(true);
            panel.hide();
        };

        // -- Public functions

        this.execute = function() {
            var random = MathUtils.randomInList(messages);
            var credits = calculateCredits(random);

            panel = PanelFactory.generate(context.state.guiLayer, PanelFactory.POPUP_SHORT, {
                title: "Event.exe",
                body: creditsMessage(random.text, credits),
                buttons: [{ text: "Accept", callback: onAccept }]
            });
            panel.show();
            panel.root.x = -panel.root.width * 0.5;

            context.creditsManaging.addCredits(credits, "event");
        };
    }

    return CreditsEarn;
});
