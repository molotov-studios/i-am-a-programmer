define(["utils/input"], function(Input) {
    function KeyStroking(context) {
        // -- Private fields

        const self = this;
        var paused = true;

        // -- Public fields

        self.onKeystroke = new Phaser.Signal();

        // -- Private functions

        var onKeyPress = function(event) {
            self.onKeystroke.dispatch(event);
        };

        // -- Public functions

        self.pause = function() {
            if(paused)
                return;
            paused = true;
            Input.onKeyUp.remove(onKeyPress);
        };

        self.resume = function() {
            if(!paused)
                return;
            paused = false;
            Input.onKeyUp.add(onKeyPress);
        };

        self.create = function() {
            // Do nothing.
        };
    }

    return KeyStroking;
});
