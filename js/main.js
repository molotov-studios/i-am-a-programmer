/*
 *   Author's note:
 *
 * I'm Daniel Herzog, and I created this piece of software. It was developed for a
 * friends-only contest, and, if you are reading this source code, please, forgive
 * me for making such a coding monstruosity. This was a quick try-and-make-stuff
 * with an unknown framework (PhaserJS) in a mediocre language such as javascript
 * (just check this link: http://mauricio.github.io/javascript-from-hell).
 *
 * Next time I'm going to use ES6 with some "browserify" tools, with linters and
 * packages managers, instead of plain javascript (ES5) with requirejs.
 *
 * Now, my advice is: leave no. Don't browse this source code. Really. It's messy,
 * buggy, and I'm not proud of it. It's just a minigame made for fun, and not a
 * maste-peese.
 *
 * Happy coding!
 */

require([
    "states/boot",
    "states/bsotd",
    "states/ending",
    "states/intro",
    "states/load",
    "states/menu",
    "states/play",
    "states/setup",
    "utils/graphics"
], function(BootState, BSOTDState, EndingState, IntroState, LoadState, MenuState, PlayState, SetupState, Graphics) {
    var game = new Phaser.Game(Graphics.SCREEN_WIDTH, Graphics.SCREEN_HEIGHT, Phaser.AUTO, "gameDiv");

    // Setup states
    game.state.add("Boot", BootState);
    game.state.add("BSOTD", BSOTDState);
    game.state.add("Ending", EndingState);
    game.state.add("Intro", IntroState);
    game.state.add("Load", LoadState);
    game.state.add("Setup", SetupState);
    game.state.add("Menu", MenuState);
    game.state.add("Play", PlayState);

    game.state.start("Boot");

    return game;
});
