define(function() {
    var Time = {
        current: function() {
            return Date.now();
        },
        millisecondsToMinutes: function(ms) {
            return ms / 60000.0;
        },
        minutesToMilliseconds: function(minutes) {
            return minutes * 60000.0;
        }
    };

    return Time;
});
