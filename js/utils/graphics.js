define(function() {
    function Graphics() {
        // -- Private fields

        const self = this;

        // -- Public fields

        self.FPS = 10;
        self.SCALE = 2.0;

        self.SCREEN_WIDTH = 800;
        self.SCREEN_HEIGHT = 600;

        self.BLACK = "#000000";
        self.WHITE = "#ffffff";
        self.BLUE = "#699EFC";
        self.GREEN = "#bce6ae";
        self.RED = "#f1c7c2";
        self.DARK_RED = "#822e24";
        self.CYAN = "#56b3c0";
        self.CYAN_DARK = "#105c68";

        self.BLACK_HEX = 0x000000;
        self.WHITE_HEX = 0xFFFFFF;
        self.RED_HEX = 0xF1C7C2;

        self.textStyle = {
            font: "24px Cave-Story",
            fill: self.WHITE,
            stroke: self.BLACK,
            strokeThickness: 4
        };

        // -- Private functions

        // ...

        // -- Public functions

        self.applyDefaultLayerOptions = function(layer) {
            layer.scale.x = layer.scale.y = self.SCALE;
            layer.x = self.SCREEN_WIDTH * 0.5;
            layer.y = self.SCREEN_HEIGHT * 0.64;
        };

        self.applyDefaultSpriteOptions = function(sprite) {
            sprite.anchor.x = sprite.anchor.y = 0.5;
            sprite.smoothed = false;
            return sprite;
        };

        self.applyDefaultGUIOptions = function(text) {
            text.anchor.y = 0.0;
            text.anchor.x = 0.0;
            text.smoothed = false;
            text.scale.x = text.scale.y = 1.0 / self.SCALE;
            return text;
        };
    }

    return new Graphics();
});
