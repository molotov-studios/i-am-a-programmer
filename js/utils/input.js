define(function() {
    function Input() {
        const self = this;

        self.onKeyUp = new Phaser.Signal();

        self.init = function(game) {
            game.input.keyboard.onUpCallback = function(event) {
                self.onKeyUp.dispatch(event);
            };
        };
    }

    return new Input();
});
