define([
    "mechanics/bug_credits_losing",
    "mechanics/bug_spawning",
    "mechanics/coding",
    "mechanics/credits_earning",
    "mechanics/credits_managing",
    "mechanics/developer_selecting",
    "mechanics/key_sequencing",
    "mechanics/key_stroking",
    "mechanics/language_selecting",
    "mechanics/persistent_shopping",
    "mechanics/random_event_calling",
    "mechanics/stats_managing",
    "mechanics/storying"
], function(
    BugCreditsLosing,
    BugSpawning,
    Coding,
    CreditsEarning,
    CreditsManaging,
    DeveloperSelecting,
    KeySequencing,
    KeyStroking,
    LanguageSelecting,
    PersistentShopping,
    RandomEventCalling,
    StatsManaging,
    Storying,
) {
    function Mechanics() {
        // -- Private fields

        const self = this;

        // -- Public fields

        self.game = null;
        self.state = null;

        self.bugCreditsLosing = new BugCreditsLosing(self);
        self.bugSpawning = new BugSpawning(self);
        self.coding = new Coding(self);
        self.creditsEarning = new CreditsEarning(self);
        self.creditsManaging = new CreditsManaging(self);
        self.developerSelecting = new DeveloperSelecting(self);
        self.keySequencing = new KeySequencing(self);
        self.keyStroking = new KeyStroking(self);
        self.languageSelecting = new LanguageSelecting(self);
        self.persistentShopping = new PersistentShopping(self);
        self.randomEventCalling = new RandomEventCalling(self);
        self.statsManaging = new StatsManaging(self);
        self.storying = new Storying(self);

        // -- Private functions

        // ...

        // -- Public functions

        self.setState = function(state) {
            self.state = state;
        };

        self.init = function(game) {
            self.game = game;

            self.bugSpawning.create();
            self.coding.create();
            self.creditsEarning.create();
            self.creditsManaging.create();
            self.developerSelecting.create();
            self.keySequencing.create();
            self.keyStroking.create();
            self.languageSelecting.create();
            self.persistentShopping.create();
            self.randomEventCalling.create();
            self.statsManaging.create();
            self.bugCreditsLosing.create();
            self.storying.create();
        };

        self.pauseIngameMechanics = function() {
            self.bugSpawning.pause();
            self.keyStroking.pause();
            self.creditsEarning.pause();
            self.randomEventCalling.pause();
            self.bugCreditsLosing.pause();
        };

        self.resumeIngameMechanics = function() {
            self.bugSpawning.resume();
            self.keyStroking.resume();
            self.creditsEarning.resume();
            self.randomEventCalling.resume();
            self.bugCreditsLosing.resume();
        };
    }

    return new Mechanics();
});
