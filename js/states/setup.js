define([
    "graphics/gui/developer_selector_panel",
    "graphics/gui/language_selector_panel",
    "graphics/gui/project_name_prompt",
    "graphics/gui/stats_meters",

    "utils/graphics",
    "utils/input",
    "utils/mechanics",
    "utils/save_data"
], function(
    GUIDeveloperSelectorPanel,
    GUILanguageSelectorPanel,
    GUIProjectNamePrompt,
    GUIStatsMeter,

    Graphics,
    Input,
    Mechanics,
    SaveData
) {
    function SetupState(game) {
        // -- Private fields

        const self = this;
        const setup_done_property_name = "_setup_done";

        const stats_meters_position = { x: -90, y: 28 };

        var stats_meter = null;

        // -- Public fields

        self.guiLayer = null;

        self.languageSelectorPanel = new GUILanguageSelectorPanel(self);
        self.developerSelectorPanel = new GUIDeveloperSelectorPanel(self);
        self.projectNamePrompt = new GUIProjectNamePrompt(self);

        // -- Private functions

        var setupBackgroundColor = function() {
            game.stage.backgroundColor = Graphics.CYAN;
        };

        var setupLayers = function() {
            self.guiLayer = game.add.group();
            Graphics.applyDefaultLayerOptions(self.guiLayer);
        };

        var setupStatsPanel = function() {
            stats_meter = new GUIStatsMeter(self);
            stats_meter.show();

            stats_meter.root.root.x = stats_meters_position.x;
            stats_meter.root.root.y = stats_meters_position.y;
        };

        var setupDone = function() {
            game.sound.play("credits1");
            SaveData.property(setup_done_property_name, true);
        };

        var isSetupDone = function() {
            return !!SaveData.property(setup_done_property_name);
        };

        var onLanguageSelected = function() {
            self.languageSelectorPanel.hide(function() {
                self.developerSelectorPanel.show(onDeveloperSelected);
            });
        };

        var onDeveloperSelected = function() {
            self.developerSelectorPanel.hide(function() {
                self.projectNamePrompt.show(onProjectNameSelected);
            });
        };

        var onProjectNameSelected = function() {
            self.projectNamePrompt.hide(function() {
                setupDone();
                game.state.start("Play");
            });
        };

        // -- Public functions

        self.preload = function() {
            // ...
        };

        self.create = function() {
            Mechanics.setState(self);

            if(isSetupDone()) {
                game.state.start("Play");
            }
            else {
                setupBackgroundColor();
                setupLayers();

                Mechanics.statsManaging.reset();
                self.languageSelectorPanel.show(onLanguageSelected);
                setupStatsPanel();
            }
        };
    }

    return SetupState;
});
