define([
    "graphics/gui/panel_factory",
    "utils/input",
    "utils/mechanics"
], function(PanelFactory, Input, Mechanics) {
    function BootState(game) {
        // -- Private fields

        const self = this;

        // -- Public functions

        self.preload = function() {
            game.load.spritesheet("loading", "assets/images/loading.png", 68, 69);
        };

        self.create = function() {
            game.add.plugin(PhaserInput.Plugin);
            game.stage.disableVisibilityChange = true;

            Input.init(game);
            Mechanics.init(game);
            PanelFactory.init(game);
            Mechanics.setState(self);

            game.camera.bounds = null;
            game.state.start("Load");
        };
    }

    return BootState;
});
