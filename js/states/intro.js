define([
    "utils/graphics"
], function(Graphics) {
    function IntroState(game) {
        // -- Private fields

        const self = this;
        const logos = [
            {
                image: "logo_molotov",
                sound: "sound_molotov"
            },
            {
                image: "logo_phaser",
                sound: null
            },
        ];
        const animation = {
            in: {
                time: 300,
                easing: Phaser.Easing.Linear.None
            },
            out: {
                delay: 2000,
                time: 350,
                easing: Phaser.Easing.Linear.None
            }
        }

        var last_image = null;

        var last_index = 0;

        // -- Public fields

        // ...

        // -- Private functions

        var onFinish = function() {
            game.state.start("Menu");
        };

        var clearCurrent = function() {
            if(last_image != null) {
                last_image.destroy();
                last_image = null;
            }
        };

        var showLogo = function(image) {
            last_image = game.add.image(0, 0, image);
            last_image.width = Graphics.SCREEN_WIDTH;
            last_image.height = Graphics.SCREEN_HEIGHT;
            last_image.smoothed = false;
            last_image.alpha = 0.0;

            // Add a tween
            var tween = game.add.tween(last_image).to(
                { alpha: 1.0 },
                animation.in.time,
                animation.in.easing
            );
            tween.onComplete.add(hideLogo);
            tween.start();
        };

        var hideLogo = function(image) {
            var tween = game.add.tween(last_image).to(
                { alpha: 0.0 },
                animation.out.time,
                animation.out.easing,
                false,
                animation.out.delay
            );
            tween.onComplete.add(showNextLogo);
            tween.start();
        };

        var playSound = function(sound) {
            if(sound)
                game.sound.play(sound);
        };

        var showNextLogo = function() {
            var logo = logos[last_index];

            if(!logo) {
                onFinish();
                return;
            }

            clearCurrent();
            showLogo(logo.image);
            playSound(logo.sound);

            last_index += 1;
        };

        // -- Public functions

        self.create = function() {
            last_index = 0;
            showNextLogo();
        };
    }

    return IntroState;
});
