define([
    "graphics/computer",
    "graphics/item_loader",
    "graphics/office_background",
    "graphics/office_desk",
    "graphics/office_window",
    "graphics/programmer",
    "graphics/screen_shaker",

    "graphics/gui/button",
    "graphics/gui/bug_spawner",
    "graphics/gui/ingame_instructions",
    "graphics/gui/shop_panel",
    "graphics/gui/status_text",
    "graphics/gui/stats_meters",
    "graphics/gui/project_panel",
    "graphics/gui/story_messages",

    "utils/graphics",
    "utils/mechanics"
], function(
    Computer,
    ItemLoader,
    OfficeBackground,
    OfficeDesk,
    OfficeWindow,
    Programmer,
    ScreenShaker,

    GUIButton,
    GUIBugSpawner,
    GUIIngameInstructions,
    GUIShopPanel,
    GUIStatusText,
    GUIStatsMeter,
    GUIProjectPanel,
    GUIStoryMessages,

    Graphics,
    Mechanics
) {
    function PlayState(game) {
        // -- Private fields

        const self = this;
        const computer = new Computer(self);
        const item_loader = new ItemLoader(self);
        const office_background = new OfficeBackground(self);
        const office_desk = new OfficeDesk(self);
        const office_window = new OfficeWindow(self);
        const programmer = new Programmer(self);
        const screen_shaker = new ScreenShaker(self);

        const bug_spawner = new GUIBugSpawner(self);
        const ingame_instructions = new GUIIngameInstructions(self);
        const shop_panel = new GUIShopPanel(self);
        const status_text = new GUIStatusText(self);
        const stats_meter = new GUIStatsMeter(self, { panel_down: true });
        const project_panel = new GUIProjectPanel(self);

        const music_volume = 0.7;

        const shop_button_position = { x: -195, y: 218 };

        var shop_button = null;
        var music = null;

        // Graphics:

        // -- Public fields

        self.game = game;

        self.backgroundLayer = null;
        self.foregroundLayer = null;
        self.guiLayer = null;

        self.storyMessages = new GUIStoryMessages(self);

        // -- Private functions

        var setupGUIBackground = function() {
            var graphics = game.make.graphics(0, 0);
            Graphics.applyDefaultSpriteOptions(graphics);

            var bounds = {
                x: -Graphics.SCREEN_WIDTH * 0.3,
                y: -47,
                width: Graphics.SCREEN_WIDTH * 0.6,
                height: 85
            }

            graphics.beginFill(Phaser.Color.hexToRGB(Graphics.CYAN), 1);
            graphics.drawRect(bounds.x, bounds.y, bounds.width, bounds.height);
            graphics.endFill();

            graphics.lineStyle(2, Phaser.Color.hexToRGB(Graphics.CYAN_DARK));
            graphics.moveTo(bounds.x - 1, bounds.y + bounds.height);
            graphics.lineTo(bounds.x + bounds.width , bounds.y + bounds.height);

            self.guiLayer.addChild(graphics);
        };

        var setupBackgroundColor = function() {
            game.stage.backgroundColor = Graphics.BLACK;
        };

        var setupLayers = function() {
            ["backgroundLayer", "foregroundLayer", "guiLayer"].forEach(function(layer) {
                self[layer] = game.add.group();
                Graphics.applyDefaultLayerOptions(self[layer]);
            });

            // Override common options
            self.guiLayer.y = Graphics.SCREEN_HEIGHT * 0.14;
        };

        var setupMechanics = function() {
            Mechanics.setState(self);

            Mechanics.resumeIngameMechanics();
            Mechanics.storying.setup();
        };

        var setupGraphics = function() {
            office_background.show();
            office_window.show();
            programmer.show();
            office_desk.show();
            computer.show();
            item_loader.show();
            screen_shaker.show();

            setupGUIBackground();
            ingame_instructions.show();
            project_panel.show();
            stats_meter.show();
            status_text.show();
            bug_spawner.show();
            self.storyMessages.show();

            shop_button = new GUIButton(game, self.guiLayer, {
                type: GUIButton.SHOP,
                callback: openShopPanel,
                mute: true
            });
            shop_button.show();
            shop_button.root.x = shop_button_position.x;
            shop_button.root.y = shop_button_position.y;
            shop_button.root.visible = Mechanics.persistentShopping.isUnlocked();

            Mechanics.persistentShopping.onUnlocked.add(function() {
                shop_button.root.visible = true;
            });
        };

        var openShopPanel = function() {
            Mechanics.pauseIngameMechanics();
            shop_panel.show(function() {
                Mechanics.resumeIngameMechanics();
            });
        };

        var playMusic = function() {
            music = game.sound.play("music");
            music.loop = true;
            music.volume = music_volume;
        };

        // -- Public functions

        self.create = function() {
            setupBackgroundColor();
            setupLayers();
            setupGraphics();
            setupMechanics();

            playMusic();
            Mechanics.randomEventCalling.enqueue();
        };

        self.update = function() {
            project_panel.update();
        };

        self.shutdown = function() {
            Mechanics.pauseIngameMechanics();
            music.destroy();
        };
    }

    return PlayState;
});
