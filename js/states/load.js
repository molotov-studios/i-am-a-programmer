define([
    "utils/input",
    "utils/graphics",
    "utils/mechanics"
], function(Input, Graphics, Mechanics) {
    function LoadState(game) {
        // -- Private fields

        const self = this;

        // NOTE: Loading screen takes up some milliseconds to load. However, since the loading
        // spinner it's extremely cool, let's keep this state for a few more extra time.
        const min_load_time = 1.0;

        var load_font_done = false;
        var timer = null;

        // -- Private functions

        var loadDone = function() {
            return load_font_done;
        };

        // -- Public functions

        self.preload = function() {
            // Show loading sprite
            var loading = game.add.sprite(
                Graphics.SCREEN_WIDTH * 0.5,
                Graphics.SCREEN_HEIGHT * 0.5,
                "loading"
            );
            Graphics.applyDefaultSpriteOptions(loading);
            loading.scale.x = loading.scale.y = Graphics.SCALE;
            loading.animations.add("spin", Phaser.ArrayUtils.numberArray(0, 15), Graphics.FPS, true);
            loading.animations.play("spin");

            timer = game.time.create(false);
            timer.start();

            // Spritesheets
            game.load.spritesheet("computer", "assets/images/computer.png", 113, 41);
            game.load.spritesheet("office_background", "assets/images/office_background.png", 400, 232);
            game.load.spritesheet("office_window", "assets/images/office_window.png", 85, 94);
            game.load.spritesheet("office_desk", "assets/images/office_desk.png", 132, 64);
            game.load.spritesheet("programmer_base", "assets/images/programmer_base.png", 72, 120);
            game.load.spritesheet("programmer_face", "assets/images/programmer_face.png", 26, 30);

            game.load.spritesheet("penguin_hat", "assets/images/penguin_hat.png", 22, 30);
            game.load.spritesheet("dumb_hat", "assets/images/dumb_hat.png", 22, 30);

            game.load.spritesheet("gui_bug", "assets/images/gui/bug.png", 41, 29);
            game.load.spritesheet("gui_button", "assets/images/gui/button.png", 74, 22);
            game.load.spritesheet("gui_button_small", "assets/images/gui/button_small.png", 48, 22);
            game.load.spritesheet("gui_button_mini", "assets/images/gui/button_mini.png", 27, 22);
            game.load.spritesheet("gui_button_shop", "assets/images/gui/button_shop.png", 36, 36);
            game.load.spritesheet("gui_meter", "assets/images/gui/meter.png", 123, 7);
            game.load.spritesheet("gui_meter_bar", "assets/images/gui/meter-bars.png", 1, 4);

            game.load.spritesheet("gui_press_enter", "assets/images/gui/press_enter.png", 55, 58);
            game.load.spritesheet("gui_press_keys", "assets/images/gui/press_keys.png", 127, 31);

            game.load.spritesheet("gui_info", "assets/images/gui/info.png", 7, 7);

            Mechanics.languageSelecting.languages.forEach(function(language) {
                name = language.name.toLowerCase();
                game.load.spritesheet(
                    `gui_language_${name}`,
                    `assets/images/gui/languages_${name}.png`,
                    120, 50
                );
            });

            Mechanics.developerSelecting.options.forEach(function(developer) {
                name = developer.filename();
                game.load.spritesheet(
                    `gui_developer_${name}`,
                    `assets/images/gui/developer_${name}.png`,
                    120, 50
                );
            });

            game.load.spritesheet("bsotd", "assets/images/bsotd.png", 800, 600);

            // Static images
            game.load.image("crash", "assets/images/crash.png");

            game.load.image("gui_canvas_large", "assets/images/gui/canvas_large.png");
            game.load.image("gui_pane_large", "assets/images/gui/pane_large.png");
            game.load.image("gui_pane_tooltip", "assets/images/gui/pane_tooltip.png");
            game.load.image("gui_sold_frame", "assets/images/gui/sold_frame.png");
            game.load.image("gui_panel", "assets/images/gui/panel.png");
            game.load.image("gui_popup", "assets/images/gui/panel_popup.png");
            game.load.image("gui_popup_large", "assets/images/gui/panel_popup_large.png");
            game.load.image("gui_popup_small", "assets/images/gui/panel_popup_small.png");
            game.load.image("gui_popup_short", "assets/images/gui/panel_popup_short.png");

            game.load.image("item_books", "assets/images/items/books.png");
            game.load.image("item_duck", "assets/images/items/duck.png");
            game.load.image("item_lego", "assets/images/items/lego.png");
            game.load.image("item_nes", "assets/images/items/nes.png");
            game.load.image("item_rubik", "assets/images/items/rubik.png");
            game.load.image("item_poster", "assets/images/items/poster.png");
            game.load.image("item_poster2", "assets/images/items/poster2.png");
            game.load.image("item_spinner", "assets/images/items/spinner.png");
            game.load.image("item_turret", "assets/images/items/turret.png");

            game.load.image("logo_molotov", "assets/images/logos/molotov.png");
            game.load.image("logo_phaser", "assets/images/logos/phaser.png");

            game.load.image("ending_bankrupt", "assets/images/endings/bankrupt.png");
            game.load.image("ending_bugged", "assets/images/endings/bugged.png");
            game.load.image("ending_lazy", "assets/images/endings/lazy.png");
            game.load.image("ending_true_programmer", "assets/images/endings/true_programmer.png");
            game.load.image("ending_pending", "assets/images/endings/pending.png");
            game.load.image("ending_grind", "assets/images/endings/grind.png");

            // Sounds
            [
                "blip1", "credits1", "popup1", "tap1", "destroy1", "bug1",
                "broken1", "install_extension", "sound_molotov", "hurt",
                "sassy", "sadness", "music", "type1"
            ].forEach(function(name) {
                game.load.audio(name, [`assets/sound/${name}.ogg`, `assets/sound/${name}.wav`]);
            });

            // Web fonts
            new FontFaceObserver('Cave-Story').load().then(function () {
              load_font_done = true;
            });
        };

        self.create = function() {
            Mechanics.setState(self);
        };

        self.update = function() {
            if(loadDone() && timer.seconds > min_load_time)
                game.state.start("Intro");
        }
    }

    return LoadState;
});
