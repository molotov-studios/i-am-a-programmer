define([
    "utils/graphics"
],
function(Graphics) {
    function OfficeBackground(state) {
        // -- Private fields

        const self = this;
        const position = { x: 0, y: 0 };
        var sprite;

        // -- Public fields

        self.root = null;

        // -- Private functions

        var addSprite = function() {
            sprite = state.game.make.sprite(position.x, position.y, "office_background");
            Graphics.applyDefaultSpriteOptions(sprite);

            state.backgroundLayer.add(sprite);

            self.root = sprite;
        };

        var animateSprite = function() {
            sprite.animations.add("idle", [0], Graphics.FPS / 2, true);
            sprite.animations.add("defeat", [1], Graphics.FPS, false);
            sprite.animations.play("idle");
        };

        // -- Public functions

        self.show = function() {
            addSprite();
            animateSprite();
        };

        self.addChild = function(child) {
            sprite.addChild(child);
        };

        self.defeat = function() {
          sprite.animations.play("defeat");
        };
    }

    return OfficeBackground;
});
