define([
    "graphics/gui/button",
    "utils/graphics"
], function(Button, Graphics) {
    function Panel(game, container, type, options) {
        // -- Private fields

        const self = this;

        const title_position = { x: 3, y: -4 };

        const popup_margin = 8;
        const popup_position = { x: popup_margin, y: popup_margin * 2 };
        const popup_buttons_margins = { x: 4, y: 0 };

        var popup_buttons_position = {};
        popup_buttons_position[PanelFactoryInstance.POPUP] = { x: 0, y: 120 };
        popup_buttons_position[PanelFactoryInstance.POPUP_LARGE] = { x: 0, y: 169 };
        popup_buttons_position[PanelFactoryInstance.POPUP_SHORT] = { x: 0, y: 72 };
        popup_buttons_position[PanelFactoryInstance.POPUP_SMALL] = { x: 0, y: 42 };

        var overlay = null;

        // -- Public fields

        self.root = null;
        self.buttons = null;

        // -- Private functions

        var addText = function(text) {
            var text_obj = game.make.text(0, 0, text, Graphics.textStyle);
            Graphics.applyDefaultGUIOptions(text_obj);
            self.root.addChild(text_obj);
            text_obj.anchor.x = 0;
            text_obj.anchor.y = 0;
            return text_obj;
        };

        var addPanelSprite = function() {
            // Create panel
            var sprite_id;

            switch(type) {
                case PanelFactoryInstance.NORMAL:
                    sprite_id = "gui_panel";
                    break;
                case PanelFactoryInstance.POPUP:
                    sprite_id = "gui_popup";
                    break;
                case PanelFactoryInstance.POPUP_LARGE:
                    sprite_id = "gui_popup_large";
                    break;
                case PanelFactoryInstance.POPUP_SHORT:
                    sprite_id = "gui_popup_short";
                    break;
                case PanelFactoryInstance.POPUP_SMALL:
                    sprite_id = "gui_popup_small";
                    break;

                default:
                    throw `Unkown panel type: ${type}`;
            }

            self.root = game.make.image(0, 0, sprite_id);
            Graphics.applyDefaultSpriteOptions(self.root);
            self.root.anchor.x = self.root.anchor.y = 0;
            container.add(self.root);

            // NOTE: This will fix bugs where behind-panel-elements are clickable.
            self.root.inputEnabled = true;

            var title = addText(options.title);
            title.x = title_position.x;
            title.y = title_position.y;
        };

        var addContent = function() {
            switch(type) {
                case PanelFactoryInstance.POPUP:
                case PanelFactoryInstance.POPUP_LARGE:
                case PanelFactoryInstance.POPUP_SMALL:
                case PanelFactoryInstance.POPUP_SHORT:
                    // Add text body
                    var body = addText(options.body);
                    body.wordWrap = true;
                    body.wordWrapWidth = (self.root.width - popup_margin * 2) * container.scale.x;
                    body.lineSpacing = -8;
                    body.x = popup_position.x;
                    body.y = popup_position.y;
                    if(options.body_size)
                        body.fontSize = options.body_size;

                    // Add popup buttons
                    if(options.buttons) {
                        var buttons = [];
                        options.buttons.forEach(function(data) {
                            var button = new Button(game, self.root, data);
                            button.show();
                            buttons.push(button);
                        });

                        // Position popup buttons
                        var total_width = (buttons.length - 1) * popup_buttons_margins.x +
                            buttons.reduce(function(acc, button) {
                                return acc + button.root.width;
                            }, 0);

                        var acc = (self.root.width - total_width) * 0.5;

                        buttons.forEach(function(button, i) {
                            button.root.x = popup_buttons_position[type].x + acc;
                            button.root.y = popup_buttons_position[type].y;

                            acc += buttons[i].root.width + popup_buttons_margins.x;
                        });
                    }

                    self.buttons = buttons;

                    // Play popup sound
                    if(!options.mute)
                        game.sound.play("popup1");

                    break;
            }
        };

        var showOverlay = function() {
            if(options.overlay_disabled)
                return;

            // NOTE: This is stupid; big enough to always cover the screen. Never do this again.
            overlay = game.make.graphics(0, 0);
            overlay.beginFill(Graphics.BLACK);
            overlay.drawRect(
                -Graphics.SCREEN_WIDTH,
                -Graphics.SCREEN_HEIGHT,
                Graphics.SCREEN_WIDTH * 2,
                Graphics.SCREEN_HEIGHT * 2
            );
            overlay.endFill();
            overlay.alpha = 0.5;
            overlay.inputEnabled = true;

            container.addChild(overlay);
        };

        var hideOverlay = function() {
            if(options.overlay_disabled)
                return;

            overlay.destroy();
        };

        // -- Public functions

        self.show = function() {
            showOverlay();
            addPanelSprite();
            addContent();
            return self;
        };

        self.hide = function() {
            self.root.destroy();
            hideOverlay();
            return self;
        };
    }


    function PanelFactory() {
        // -- Private fields

        const self = this;
        var game;

        // -- Public fields

        self.NORMAL = "NORMAL";
        self.POPUP = "POPUP";
        self.POPUP_SHORT = "POPUP_SHORT";
        self.POPUP_LARGE = "POPUP_LARGE";
        self.POPUP_SMALL = "POPUP_SMALL";

        // -- Private functions

        // -- Public functions

        self.init = function(game_ref) {
            game = game_ref;
        };

        self.generate = function(container, type, options) {
            return new Panel(game, container, type, options);
        };
    }

    var PanelFactoryInstance = new PanelFactory();
    return PanelFactoryInstance;
});
