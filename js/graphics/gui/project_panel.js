define([
    "graphics/gui/panel_factory",
    "utils/graphics",
    "utils/mechanics",
    "utils/save_data"
], function(PanelFactory, Graphics, Mechanics, SaveData) {
    function ProjectName(game, parent) {
        // -- Private fields

        const self = this;
        const project_name_property = "_project_name";
        const data = {
            x: 6,
            y: 6,
            size: '40px',
            stroke: 5
        };

        // -- Private functions

        var fetchText = function() {
            return SaveData.property(project_name_property);
        };

        var showText = function() {
            var text = game.make.text(data.x, data.y, fetchText(), Graphics.textStyle);
            Graphics.applyDefaultGUIOptions(text);
            text.fontSize = data.size;
            text.strokeThickness = data.stroke;
            // text.anchor.x = text.anchor.y = 0.5;

            parent.addChild(text);
        };

        // -- Public functions

        self.show = function() {
            showText();
        };
    }

    function ProgrammerInfo(game, parent) {
        // -- Private fields

        const self = this;
        const language_property = "_choice_language";
        const developer_property = "_choice_developer";
        const data = {
            x: 6,
            y: 30,
            size: '24px',
            lineSpacing: -10
        };

        // -- Private functions

        var fetchText = function() {
            var developer = SaveData.property(developer_property).replace(/\s+/g, ' ');
            var language = SaveData.property(language_property).replace(/\s+/g, ' ');

            return `${language}\n${developer}`;
        };

        var showText = function() {
            var text = game.make.text(data.x, data.y, fetchText(), Graphics.textStyle);
            Graphics.applyDefaultGUIOptions(text);
            text.fontSize = data.size;
            text.lineSpacing = data.lineSpacing;

            parent.addChild(text);
        };

        // -- Public functions

        self.show = function() {
            showText();
        };
    }

    function Money(game, parent) {
        // -- Private fields

        const self = this;
        const data = {
            x: 180,
            y: 25,
            size: '52px',
            stroke: 5,
            color: Graphics.WHITE
        };
        const animation_credits_per_frame = 7;

        var text = null;
        var current_value = null;

        // -- Private functions

        var creditsPerFrame = function() {
            var diff = Math.abs(current_value - Mechanics.creditsManaging.credits());
            return Math.max(diff * 0.05, animation_credits_per_frame);
        };

        var fetchText = function() {
            return `${Math.floor(current_value)}$`
        };

        var showText = function() {
            text = game.make.text(data.x, data.y, fetchText(), Graphics.textStyle);
            Graphics.applyDefaultGUIOptions(text);
            text.anchor.x = 1;
            text.anchor.y = 0;
            text.fontSize = data.size;
            text.strokeThickness = data.stroke;
            updateText();

            parent.addChild(text);
        };

        var updateText = function() {
            text.text = fetchText();

            if(current_value < 0)
                text.fill = Graphics.RED;
            else
                text.fill = data.color;
        };

        // -- Public functions

        self.update = function() {
            var real = Mechanics.creditsManaging.credits();

            if(current_value < real) {
                current_value += creditsPerFrame();
                if(current_value > real)
                    current_value = real;

                updateText();
            }
            else if(current_value > real) {
                current_value -= creditsPerFrame();
                if(current_value < real)
                    current_value = real;

                updateText();
            }
        };

        self.show = function() {
            current_value = Mechanics.creditsManaging.credits();
            showText();
        };
    }

    function ProjectPanel(state) {
        // -- Private fields

        const self = this;

        const panel_title = "Project.exe";
        const panel_margin = { x: 2, y: -2 }

        var panel;
        var project_name;
        var programmer_info;
        var money;

        // -- Private functions

        var addPanel = function() {
            panel = PanelFactory.generate(
                state.guiLayer,
                PanelFactory.NORMAL,
                {
                    title: panel_title,
                    overlay_disabled: true
                }
            );
            panel.show();
            panel.root.x = panel_margin.x;
            panel.root.y = -panel.root.height * 0.5 + panel_margin.y;
        };

        var addElements = function() {
            project_name = new ProjectName(state.game, panel.root);
            programmer_info = new ProgrammerInfo(state.game, panel.root);
            money = new Money(state.game, panel.root);

            project_name.show();
            programmer_info.show();
            money.show();
        }

        var addSprites = function() {
            addPanel();
            addElements();
        };

        // -- Public functions

        self.show = function() {
            addSprites();
        };

        self.update = function() {
            money.update();
        };
    }

    return ProjectPanel;
});
