define([
    "graphics/gui/panel_factory",
    "utils/graphics",
    "utils/input",
    "utils/save_data"
], function(PanelFactory, Graphics, Input, SaveData) {
    function ProjectNamePrompt(state) {
        // -- Private fields

        const self = this;

        const project_name_property = "_project_name";

        const input_length = {
            min: 3,
            max: 20
        };
        const input_settings = {
            x: 10,
            y: 58,
            width: 440,
            font: '52px Cave-Story',
            fill: Graphics.BLACK,
            fontWeight: 'bold',
            padding: 16,
            borderWidth: 2,
            borderColor: Graphics.BLACK,
            borderRadius: 4,
            placeHolder: 'Write here',
            max: input_length.max
        };
        // const input_regex = /[^a-z0-9_\-+\/:;'".,$!?*()\[\]{}@$&%|\\ ]/ig;

        var popup;
        var input;

        var onDoneCallback;

        // -- Public fields

        self.inputValue = null;

        // -- Private functions

        // var correctInput = function() {
        //     input.setText(input.value.replace(input_regex, ''));
        // };

        var validInput = function() {
            return input.value.length >= input_length.min && input.value.length <= input_length.max;
        };

        var onDone = function() {
            if(validInput()) {
                SaveData.property(project_name_property, input.value);
                onDoneCallback();
            }
        };

        var setupPopup = function() {
            var options = {
                title: "ProjectName.exe",
                body: `Please, select a cool name for your project (from ${input_length.min} ` +
                      `to ${input_length.max} characters) :`,
                overlay_disabled: true,
                buttons: [
                    { text: "Accept", callback: onDone, mute: true, disabled: true }
                ]
            };
            popup = PanelFactory.generate(state.guiLayer, PanelFactory.POPUP, options);
            popup.show();
            popup.root.x = -popup.root.width * 0.5;
            popup.root.y = -popup.root.height;
        };

        var setupInput = function() {
            input = state.game.make.inputField(input_settings.x, input_settings.y, input_settings);
            Graphics.applyDefaultSpriteOptions(input);
            input.anchor.x = input.anchor.y = 0;
            input.scale.x = input.scale.y = 1.0 / Graphics.SCALE;

            popup.root.addChild(input);
        };

        var onChange = function() {
            // correctInput();

            if(validInput())
                popup.buttons[0].enable();
            else
                popup.buttons[0].disable();
        };

        // -- Public functions

        self.show = function(callback) {
            onDoneCallback = callback;

            setupPopup();
            setupInput();
            onChange();

            Input.onKeyUp.add(onChange);
        };

        self.hide = function(onHide) {
            popup.hide();
            onHide();
        };
    }

    return ProjectNamePrompt;
});
