define([
    "utils/graphics",
    "utils/math",
    "utils/mechanics"
], function(Graphics, MathUtils, Mechanics) {
    function Bug(game, parent, callback) {
        // -- Private fields

        const self = this;
        const sprite_id = "gui_bug";
        const margins = { x: { min: -150, max: 150 }, y: { min: -10, max: 220 } };
        const offest = { x: { min: -2, max: 2 }, y: { min: -2, max: 2 } };
        const vibration_delay = 200;

        var sprite = null;
        var sprite_base_position = null;
        var vibration_timer = null;

        // -- Public fields

        // ...

        // -- Private functions

        var setupBasePosition = function() {
            sprite_base_position = {
                x: MathUtils.randomIntInRange(margins.x.min, margins.x.max),
                y: MathUtils.randomIntInRange(margins.y.min, margins.y.max)
            };

            sprite.x = sprite_base_position.x;
            sprite.y = sprite_base_position.y;
        };

        var setupSprite = function() {
            sprite = game.make.sprite(0, 0, sprite_id);
            Graphics.applyDefaultSpriteOptions(sprite);
            sprite.inputEnabled = true;
            sprite.input.useHandCursor = true;
            parent.addChild(sprite);
        };

        var setupAnimations = function() {
            sprite.animations.add("idle", [0, 1, 2, 3], Graphics.FPS, true);
            sprite.animations.add("kill", [4, 5, 6, 7], Graphics.FPS, false);
            sprite.animations.play("idle");
            sprite.animations.currentAnim.setFrame(MathUtils.randomIntInRange(0, 4), true);

            // Add a timer
            vibration_timer = game.time.create(false);
            vibration_timer.loop(vibration_delay, randomizePosition);
            vibration_timer.start();
        };

        var setupEvents = function() {
            sprite.events.onInputDown.add(onClick);
        };

        var onClick = function() {
            callback(self);
        };

        var randomizePosition = function() {
            var offset = {
                x: MathUtils.randomIntInRange(offest.x.min, offest.x.max),
                y: MathUtils.randomIntInRange(offest.y.min, offest.y.max)
            };

            sprite.x = sprite_base_position.x + offset.x;
            sprite.y = sprite_base_position.y + offset.y;
        };

        // -- Public functions

        self.show = function() {
            setupSprite();
            setupAnimations();
            setupBasePosition();
            setupEvents();
        };

        self.destroy = function() {
            sprite.events.onInputDown.remove(onClick);
            sprite.inputEnabled = false;
            sprite.animations.play("kill").onComplete.add(function() {
                sprite.destroy();
                vibration_timer.destroy();
            });
        };
    }

    function BugSpawner(state) {
        // -- Private fields

        const self = this;

        var layer = null;

        // -- Public fields

        // ...

        // -- Private functions

        var onBugClick = function(bug) {
            bug.destroy();
            Mechanics.bugSpawning.solve();
            state.game.sound.play("destroy1");
        };

        var spawnBug = function(count) {
            for(var i = 0; i < count; ++i) {
                var newbug = new Bug(state.game, layer, onBugClick);
                newbug.show();
            }
            state.game.sound.play("bug1");
        };

        var setupLayers = function() {
            layer = state.game.make.group();
            state.guiLayer.addChild(layer);
        };

        var setupEvents = function() {
            Mechanics.bugSpawning.onNewBug.add(spawnBug);
        };

        // -- Public functions

        self.show = function() {
            setupLayers();
            setupEvents();
        };
    }

    return BugSpawner;
});
