define([
    "utils/graphics"
], function(Graphics) {
    function Button(game, container, options) {
        // -- Private fields

        const self = this;

        var text = null;

        // -- Public fields

        self.root = null;
        self.disabled = false;

        // -- Private functions

        var setupSprite = function() {
            var type;
            switch(options.type) {
                case Button.MINI:
                    type = "gui_button_mini";
                    break;
                case Button.SMALL:
                    type = "gui_button_small";
                    break;
                case Button.SHOP:
                    type = "gui_button_shop";
                    break;
                default:
                    type = "gui_button";
            }

            self.root = game.make.button(0, 0, type);
            Graphics.applyDefaultSpriteOptions(self.root);
            self.root.anchor.x = self.root.anchor.y = 0.0;
            self.root.input.useHandCursor = true;

            self.root.animations.add("out", [0], Graphics.FPS);
            self.root.animations.add("over", [1], Graphics.FPS);
            self.root.animations.add("down", [2], Graphics.FPS);
            self.root.animations.add("disabled", [3], Graphics.FPS);
            self.root.animations.play("out");

            container.addChild(self.root);
        };

        var setupEvents = function() {
            self.root.onInputOver.add(onButtonOver, this);
            self.root.onInputOut.add(onButtonOut, this);
            self.root.onInputDown.add(onButtonDown, this);
            self.root.onInputUp.add(onButtonUp, this);
        };

        var setupText = function() {
            text = game.make.text(0, 0, options.text, Graphics.textStyle);
            Graphics.applyDefaultGUIOptions(text);
            text.anchor.x = text.anchor.y = 0.5;
            text.strokeThickness = 0;
            text.fill = Graphics.BLACK;

            text.x = self.root.width * 0.5;
            text.y = self.root.height * 0.5;

            self.root.addChild(text);
        };

        var onButtonOver = function() {
            if(self.disabled)
              return;

            self.root.animations.play("over");
            text.fill = Graphics.BLACK;
        };

        var onButtonOut = function() {
            if(self.disabled)
              return;

            self.root.animations.play("out");
            text.fill = Graphics.BLACK;
        };

        var onButtonDown = function() {
            if(self.disabled)
              return;

            self.root.animations.play("down");
            text.fill = Graphics.WHITE;
        };

        var onButtonUp = function() {
            if(self.disabled)
              return;

            self.root.animations.play("out");
            text.fill = Graphics.BLACK;

            options.callback();
            if(!options.mute)
                game.sound.play("blip1");
        };

        // -- Public functions

        self.enable = function() {
            if(!self.disabled)
                return;

            self.disabled = false;
            self.root.animations.play("out");
            self.root.input.enabled = true;
            text.fill = Graphics.BLACK;
        };

        self.disable = function() {
            if(self.disabled)
                return;

            self.disabled = true;
            self.root.animations.play("disabled");
            self.root.input.enabled = false;
            text.fill = Graphics.BLACK;
        };

        self.show = function() {
            setupSprite();
            setupText();
            setupEvents();
        };
    }

    Button.NORMAL = "normal";
    Button.SMALL = "small";
    Button.MINI = "mini";
    Button.SHOP = "shop";

    return Button;
});
