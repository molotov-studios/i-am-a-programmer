define([
    "graphics/gui/button",
    "graphics/gui/panel_factory",
    "utils/graphics",
    "utils/mechanics"
], function(
    Button,
    PanelFactory,
    Graphics,
    Mechanics
) {
    const ITEMS_PER_PAGE = 4;

    function ShopItemPopup(state, data, callback) {
        // -- Private fields

        const self = this;
        const popup_options = {
            title: "ConfirmPurchase.exe",
            body: `${data.title.toUpperCase()}\n\n${data.description}\n\n` +
                  `Are you sure that you want to spend ${data.price} $ to buy it?`,
            buttons: [
                { text: "Accept", mute: true, callback: function() { onConfirm(); } },
                { text: "Cancel", callback: function() { onCancel(); } }
            ]
        }
        const position = { x: 15, y: 45 };

        var popup = null;

        // -- Private functions

        var onConfirm = function() {
            Mechanics.persistentShopping.purchase(data);
            self.hide();
            callback();
        };

        var onCancel = function() {
            self.hide();
            callback();
        };

        var setupPopup = function() {
            popup = PanelFactory.generate(state.guiLayer, PanelFactory.POPUP, popup_options);
            popup.show();

            popup.root.x = position.x - popup.root.width * 0.5;
            popup.root.y = position.y;
        };

        // -- Public functions

        self.show = function() {
            setupPopup();
        };

        self.hide = function() {
            popup.hide();
        };
    }

    function ShopItem(state, parent, data, index, callback) {
        // -- Private fields

        const self = this;
        const positions = [
            { x:  11, y:  7 }, // 0
            { x: 123, y:  7 }, // 1
            { x:  11, y: 65 }, // 2
            { x: 123, y: 65 }, // 3
        ];
        const thumbnail_position = { x: 6, y: 20 };
        const name_position = { x: 51, y: 7 };
        const price_position = { x: 66, y: 20 };
        const buy_button_position = { x: 41, y: 29 };
        const sold_overlay_position = { x: 4, y: 4 };
        const sold_text_data = {
            x: 52,
            y: 22,
            text: "SOLD",
            size: '90px',
            color: Graphics.DARK_RED
        };

        var pane = null;

        var thumbnail = null;
        var name = null;
        var price = null;
        var buy_button = null;

        var sold_overlay = null;
        var sold_text = null;

        // -- Private functions

        var showBuyPopup = function() {
            var popup = new ShopItemPopup(state, data, function() {
                self.redraw();
                callback();
            });
            popup.show();
        };

        var setupThumbnail = function() {
            var image_id = data.display.image;
            thumbnail = state.game.make.image(thumbnail_position.x, thumbnail_position.y, image_id);
            Graphics.applyDefaultSpriteOptions(thumbnail);
            thumbnail.anchor.x = thumbnail.anchor.y = 0;
            pane.addChild(thumbnail);
        };

        var setupName = function() {
            name = state.game.make.text(0, 0, data.title, Graphics.textStyle);
            Graphics.applyDefaultGUIOptions(name);
            name.anchor.x = name.anchor.y = 0.5;
            name.x = name_position.x;
            name.y = name_position.y;

            pane.addChild(name);
        };

        var setupPrice = function() {
            price = state.game.make.text(
                price_position.x,
                price_position.y,
                `${data.price} $`,
                Graphics.textStyle
            );
            Graphics.applyDefaultGUIOptions(price);
            price.anchor.x = price.anchor.y = 0.5;
            price.fontSize = '34px';

            pane.addChild(price);

            if(Mechanics.persistentShopping.canBuy(data))
                price.fill = Graphics.GREEN;
            else
                price.fill = Graphics.RED;
        };

        var setupBuyButton = function() {
            var options = { text: "Buy", callback: showBuyPopup, type: Button.SMALL, mute: true };
            buy_button = new Button(state.game, pane, options);
            buy_button.show();
            buy_button.root.x = buy_button_position.x;
            buy_button.root.y = buy_button_position.y;

            if(!Mechanics.persistentShopping.canBuy(data))
                buy_button.disable();
        };

        var setupSoldOverlay = function() {
            if(!Mechanics.persistentShopping.isItemUnlocked(data))
                return;

            // Add overlay
            sold_overlay = state.game.make.image(
                sold_overlay_position.x,
                sold_overlay_position.y, "gui_sold_frame"
            );
            Graphics.applyDefaultSpriteOptions(sold_overlay);
            sold_overlay.anchor.x = sold_overlay.anchor.y = 0;
            pane.addChild(sold_overlay);

            // Add text
            sold_text = state.game.make.text(
                sold_text_data.x,
                sold_text_data.y,
                sold_text_data.text,
                Graphics.textStyle
            );
            Graphics.applyDefaultGUIOptions(sold_text);
            sold_text.anchor.x = sold_text.anchor.y = 0.5;
            sold_text.fontSize = sold_text_data.size;
            sold_text.strokeThickness = 0;
            sold_text.fill = sold_text_data.color;

            pane.addChild(sold_text);

            var sold_text = sold_text
        };

        var setupPane = function() {
            pane = state.game.make.image(0, 0, "gui_pane_large");
            Graphics.applyDefaultSpriteOptions(pane);
            pane.anchor.x = pane.anchor.y = 0;
            parent.addChild(pane);
        };

        var reposition = function() {
            pane.x = positions[index].x;
            pane.y = positions[index].y;
        };

        // -- Public functions

        self.create = function() {
            setupPane();
            setupThumbnail();
            setupName();
            setupPrice();
            setupBuyButton();
            setupSoldOverlay();
            reposition();
        };

        self.destroy = function() {
            pane.destroy();
        };

        self.redraw = function() {
            self.destroy();
            self.create();
        };
    }

    function ShopItems(state, parent, on_buy) {
        // -- Private fields

        const self = this;
        const position = { x: 10, y: 35 };

        var visual_items = [];

        // -- Public fields

        self.root = null;
        self.page = 0;

        // -- Private functions

        var setupGroup = function() {
            self.root = state.game.make.image(position.x, position.y, "gui_canvas_large");
            Graphics.applyDefaultSpriteOptions(self.root);
            self.root.anchor.x = self.root.anchor.y = 0;
            parent.addChild(self.root);
        };

        var showItemsList = function() {
            visual_items.forEach(function(item) {
                item.destroy();
            });
            visual_items = [];
            self.root.removeChildren();

            var items = currentDataItems();
            for(var i in items) {
              var item = new ShopItem(state, self.root, items[i], i, on_buy);
              item.create();
              visual_items.push(item);
            }
        };

        var currentDataItems = function() {
            return Mechanics.persistentShopping.items.slice(
                self.page * ITEMS_PER_PAGE, (self.page + 1) * ITEMS_PER_PAGE
            );
        };

        // -- Public functions

         self.create = function() {
             setupGroup();
             showItemsList();
         };

         self.update = function() {
             showItemsList();
         };
    }

    function ShopPanel(state) {
        // -- Private fields

        const self = this;
        var panel = null;
        var on_close = null;
        var items_list = null;

        var current_page = 0;

        // -- Private functions

        var firstPage = function() {
            return 0;
        };

        var lastPage = function() {
            return Math.floor((Mechanics.persistentShopping.items.length - 1) / ITEMS_PER_PAGE);
        };

        var updateNavigationButtons = function() {
            panel.buttons[0].enable();
            panel.buttons[2].enable();

            if(current_page == firstPage()) {
                panel.buttons[0].disable();
            }
            else if(current_page == lastPage()) {
                panel.buttons[2].disable();
            }
        };

        var onNext = function() {
            if(current_page + 1 <= lastPage())
                current_page += 1;

            updateNavigationButtons();
            items_list.page = current_page;
            items_list.update();
        };

        var onPrevious = function() {
            if(current_page - 1 >= firstPage())
                current_page -= 1;

            updateNavigationButtons();
            items_list.page = current_page;
            items_list.update();
        };

        var setupPanel = function() {
            var options = {
                title: "Shop.exe",
                body: "Welcome to the shop! Everything is expensive as hell.",
                buttons: [
                    { text: "<", type: Button.MINI, callback: onPrevious },
                    { text: "Close", callback: self.hide },
                    { text: ">", type: Button.MINI, callback: onNext }
                ]
            };

            panel = PanelFactory.generate(state.guiLayer, PanelFactory.POPUP_LARGE, options);
            panel.show();
            panel.root.x = -panel.root.width * 0.5;
        };

        var onBuy = function() {
            self.hide();
        };

        var setupItemsList = function() {
            current_page = 0;
            items_list = new ShopItems(state, panel.root, onBuy);
            items_list.create();
        };

        // -- Public functions

        self.show = function(callback) {
            on_close = callback;
            current_page = 0;

            setupPanel();
            updateNavigationButtons();
            setupItemsList();
        };

        self.hide = function() {
            panel.hide();
            on_close();
        };
    }

    return ShopPanel;
});
