define([
    "graphics/gui/panel_factory",
    "utils/graphics",
    "utils/mechanics"
], function(PanelFactory, Graphics, Mechanics) {
    function StatsMeter(state, options) {
        // -- Private fields

        if(!options)
            options = {};

        const self = this;

        const panel_title = "Stats.exe";
        const panel_margin = { x: -2, y: -2 }

        const meter_offsets = { x: 61, y: 18 };
        const meter_offsets_step_y = 16;

        const meter_bar_offset = { x: 3, y: 1 };
        const meter_bar_width = 109;

        const info_button_offset = { x: 5 };

        const animation_time = 500;
        const animation_easing = Phaser.Easing.Quadratic.InOut;

        const font_size = '22px';

        const panel_title_data = { x: -146, y: -62, size: '28px' };
        const panel_text_data = { x: -146, y: -45, size: '28px', width: 285, spacing: -10 };

        const panel_down_offset = { x: 154, y: 65 };

        const info_panel_offset = { x: 120, y: -2 };

        const tutorials = {
            happiness: "The more happy you are, the more random events will occur!\nYou may lose the light bar section.",
            concentration: "Too many bugs? You just need to concentrate more to avoid them!\nYou may lose the light bar section.",
            productivity: "Want to earn more money! Then be more productive!\nYou may lose the light bar section."
        };

        const colors = {
            blue: [0, 1],
            red: [2, 3],
            green: [4, 5]
        };

        // -- Public fields

        self.root = null;

        // -- Private functions

        var addText = function(parent, text) {
            var text = state.game.make.text(0, 0, text, Graphics.textStyle);
            Graphics.applyDefaultGUIOptions(text);
            parent.addChild(text);
            text.fontSize = font_size;
            return text;
        };

        var addPanel = function() {
            var panel = PanelFactory.generate(
                state.guiLayer,
                PanelFactory.NORMAL,
                {
                    title: panel_title,
                    overlay_disabled: true
                }
            );
            panel.show();
            panel.root.x = -panel.root.width + panel_margin.x;
            panel.root.y = -panel.root.height * 0.5 + panel_margin.y;

            self.root = panel;
        };

        var addInfoPanel = function(parent, stat, color) {
            // Setup info button

            var info_button = state.game.make.sprite(parent.width + info_button_offset.x, 0, "gui_info");
            Graphics.applyDefaultSpriteOptions(info_button);
            info_button.anchor.y = 0;
            info_button.inputEnabled = true;

            info_button.animations.add("out", [0], 0);
            info_button.animations.add("over", [1], 0);
            info_button.animations.play("out");

            parent.addChild(info_button);

            info_button.events.onInputOver.add(function() {
                info_panel.visible = true;
                info_button.animations.play("over");
            });

            info_button.events.onInputOut.add(function() {
                info_panel.visible = false;
                info_button.animations.play("out");
            });

            // Setup info panel

            var info_panel = state.game.make.image(info_panel_offset.x, info_panel_offset.y,
                                                   "gui_pane_tooltip");
            Graphics.applyDefaultSpriteOptions(info_panel);
            info_panel.visible = false;
            info_panel.anchor.x = info_panel.anchor.y = 1;
            parent.addChild(info_panel);

            // Setup panel title
            var panel_title = state.game.make.text(panel_title_data.x, panel_title_data.y,
                                                   stat.prettyName(), Graphics.textStyle);
            Graphics.applyDefaultGUIOptions(panel_title);
            panel_title.fontSize = panel_title_data.size;
            panel_title.anchor.x = 0;
            panel_title.fill = Graphics[color.toUpperCase()];
            info_panel.addChild(panel_title);

            // Setup panel text
            var panel_text = state.game.make.text(panel_text_data.x, panel_text_data.y,
                                                  tutorials[stat.name], Graphics.textStyle);
            Graphics.applyDefaultGUIOptions(panel_text);
            panel_text.anchor.x = 0;
            panel_text.lineSpacing = panel_text_data.spacing;
            panel_text.wordWrapWidth = panel_text_data.width;
            panel_text.wordWrap = true;
            info_panel.addChild(panel_text);

            if(options.panel_down) {
                info_panel.x += panel_down_offset.x;
                info_panel.y += panel_down_offset.y;
            }
        };

        var addMeterBar = function(color, stat_name) {
            var meter = state.game.make.sprite(meter_offsets.x, meter_offsets.y, "gui_meter");
            Graphics.applyDefaultSpriteOptions(meter);
            meter.anchor.x = meter.anchor.y = 0;
            self.root.root.addChild(meter);

            var stat = Mechanics.statsManaging[stat_name];

            // Create threshold bar
            var threshold_bar = state.game.make.sprite(
                meter_bar_offset.x,
                meter_bar_offset.y,
                "gui_meter_bar"
            );
            Graphics.applyDefaultSpriteOptions(threshold_bar);
            threshold_bar.anchor.x = threshold_bar.anchor.y = 0;
            threshold_bar.width = meter_bar_width * (stat.thresholdValue() || 0.01);
            threshold_bar.blendMode = Phaser.blendModes.MULTIPLY;
            threshold_bar.frame = colors[color][0];
            meter.addChild(threshold_bar);

            // Create extra bar
            var extra_bar = state.game.make.sprite(
                meter_bar_offset.x + threshold_bar.width,
                meter_bar_offset.y,
                "gui_meter_bar"
            );
            Graphics.applyDefaultSpriteOptions(extra_bar);
            extra_bar.anchor.x = extra_bar.anchor.y = 0;
            extra_bar.width = meter_bar_width * (stat.extraValue() || 0.01);
            extra_bar.blendMode = Phaser.blendModes.MULTIPLY;
            extra_bar.frame = colors[color][1];
            meter.addChild(extra_bar);

            // Add event listeners
            stat.onThresholdChange.add(function(_stat, threshold) {
                var threshold_target_width = meter_bar_width * threshold;
                state.game.add.tween(threshold_bar)
                    .to({ width: threshold_target_width },
                        animation_time, animation_easing, true);

                state.game.add.tween(extra_bar)
                    .to({ x: meter_bar_offset.x + threshold_target_width },
                        animation_time, animation_easing, true);
            });

            stat.onExtraChange.add(function(_stat, extra) {
                var extra_target_width = meter_bar_width * extra;
                state.game.add.tween(extra_bar)
                    .to({ width: extra_target_width },
                        animation_time, animation_easing, true);
            });

            var text = addText(meter, stat.prettyName());
            text.anchor.x = 1.02;
            text.anchor.y = 0.35;

            addInfoPanel(meter, stat, color);

            return meter;
        };

        var addMeterBars = function() {
            var happiness = addMeterBar("red", "happiness");
            var productivity = addMeterBar("blue", "productivity");
            var concentration = addMeterBar("green", "concentration");

            productivity.y += meter_offsets_step_y;
            concentration.y += meter_offsets_step_y * 2;
        };

        var addSprites = function() {
            addPanel();
            addMeterBars();
        };

        // -- Public functions

        self.show = function() {
            addSprites();
        };
    }

    return StatsMeter;
});
