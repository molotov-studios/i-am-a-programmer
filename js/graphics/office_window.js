define([
    "utils/graphics"
], function(Graphics) {
    function OfficeDesk(state) {
        // -- Private fields

        const self = this;
        const position = { x: 62, y: -69 };

        var sprite;

        // -- Public fields

        self.root = null;

        // -- Private functions

        var addSprite = function() {
            sprite = state.game.make.sprite(position.x, position.y, "office_window");
            Graphics.applyDefaultSpriteOptions(sprite);
            sprite.anchor.x = sprite.anchor.y = 0;

            state.backgroundLayer.add(sprite);

            sprite.animations.add("idle", Phaser.ArrayUtils.numberArray(0, 19),
                                  Graphics.FPS / 2, true);
            sprite.animations.play("idle");

            self.root = sprite;
        };

        // -- Public functions

        self.show = function() {
            addSprite();
        };
    }

    return OfficeDesk;
});
