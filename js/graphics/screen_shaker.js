define([
    "utils/math",
    "utils/mechanics"
], function(MathUtils, Mechanics) {
    function ScreenShaker(state) {
        // -- Private fields

        const self = this;
        const diff = { x: 5, y: 5 };
        const animation_time = 50;
        const animation_easing = Phaser.Easing.Linear.None;

        var game = null;

        // -- Private functions

        var randomPoint = function() {
            return {
                x: MathUtils.randomInRange(-diff.x, diff.x),
                y: MathUtils.randomInRange(-diff.y, diff.y)
            };
        };

        var shake = function() {
            var point = randomPoint();

            game.camera.x = point.x;
            game.camera.y = point.y;

            game.add.tween(game.camera).to({ x: 0, y: 0 }, animation_time, animation_easing, true);
        };

        var setupEvents = function() {
            Mechanics.coding.onLineChange.add(shake);
            Mechanics.coding.onLineCompleted.add(shake);
        };

        // -- Public functions

        self.show = function() {
            game = state.game;
            // Do nothing visual
            setupEvents();
        };
    }

    return ScreenShaker;
});
